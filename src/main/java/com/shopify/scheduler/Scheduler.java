package com.shopify.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.shopify.service.ProductService;
@Component
public class Scheduler {
	@Autowired
	private ProductService productService;
	
	@Transactional
	@Scheduled(cron = "0 0/5 * 1/1 * ?")
	   public void cronJobSch() {
	      String message=productService.getProducts();
	      System.out.println(message);
	   }
}
