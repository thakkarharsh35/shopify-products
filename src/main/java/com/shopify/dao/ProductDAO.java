package com.shopify.dao;

import org.springframework.data.repository.CrudRepository;

import com.shopify.model.ProductVO;


public interface ProductDAO extends  CrudRepository<ProductVO, Integer>{

}
