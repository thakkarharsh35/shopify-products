package com.shopify.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.shopify.dao.ProductDAO;
import com.shopify.model.ProductVO;
import com.shopify.model.Products;
import com.shopify.model.Variant;
import com.shopify.util.Constants;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ProductDAO productDAO;

	Map<String, ProductVO> products = new HashMap<>();

	@Override
	public String getProducts() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.set(Constants.ACCESS_TOKEN_STRING, Constants.PVT_AUTH_TOKEN);

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<Products> response = restTemplate.exchange(Constants.URI, HttpMethod.GET, entity,
				Products.class);
		Products allProducts = response.getBody();
		List<ProductVO> productList = allProducts.getProducts();
		for (ProductVO product : productList) {
			if (!product.getVariants().isEmpty()) {
				List<Variant> variantList = product.getVariants();

				for (Variant variant : variantList) {

					List<Variant> singleVariantList = new ArrayList<>();
					singleVariantList.add(variant);

					ProductVO productWithVariant = new ProductVO();

					productWithVariant.setId(product.getId());
					productWithVariant.setVendor(product.getVendor());
					productWithVariant.setTitle(product.getTitle());
					productWithVariant.setCreated_at(product.getCreated_at());
					productWithVariant.setProduct_type(product.getProduct_type());
					productWithVariant.setProduct(product);
					productWithVariant.setVariants(singleVariantList);
					product.getChildren().add(productWithVariant);

					save(productWithVariant);
				}

			} else {
				save(product);
			}
		}

		return "Products are fetched";
	}

	@Transactional
	public void save(ProductVO product) {
		productDAO.save(product);
	}
}
