package com.shopify.model;

import java.util.List;

public class Products {
	
	private List<ProductVO> products;

	public List<ProductVO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductVO> products) {
		this.products = products;
	}
	
}
