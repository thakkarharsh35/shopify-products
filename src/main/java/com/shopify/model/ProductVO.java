package com.shopify.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Product")
public class ProductVO {
	
	@Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
	private long uniqueId;
	/*
	 * @GeneratedValue(generator = "uuid2")
	 * 
	 * @GenericGenerator(name = "uuid2", strategy = "uuid2")
	 * 
	 * @Column(columnDefinition = "BINARY(16)")
	 */
	
	
	private String id;
	
	private String title;
	
	private String vendor;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "Product_Id")
	private ProductVO product;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "product",cascade = CascadeType.ALL)
	private Set<ProductVO> children = new HashSet<>();
	
    @Convert(converter=VariantToJsonConverter.class)
    @Column(length = 4000)
	private List<Variant> variants;
    
    private String product_type;
    
    private String created_at;

	public long getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(long uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public ProductVO getProduct() {
		return product;
	}

	public void setProduct(ProductVO product) {
		this.product = product;
	}

	public Set<ProductVO> getChildren() {
		return children;
	}

	public void setChildren(Set<ProductVO> children) {
		this.children = children;
	}

	public List<Variant> getVariants() {
		return variants;
	}

	public void setVariants(List<Variant> variants) {
		this.variants = variants;
	}

	public String getProduct_type() {
		return product_type;
	}

	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
		
}
