package com.shopify.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.persistence.AttributeConverter;

import org.springframework.boot.json.JsonParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class VariantToJsonConverter implements AttributeConverter<List<Variant>, String> {

	ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(List<Variant> variant) {
		String json = "";
		try {
			json = objectMapper.writeValueAsString(variant);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}

	@Override
	public List<Variant> convertToEntityAttribute(String variantAsJson) {
		List<Variant> variant = null;
		try {
			variant=Arrays.asList(objectMapper.readValue(variantAsJson, Variant[].class));
		} catch (JsonParseException | IOException e) {
			e.printStackTrace();
		}
		return variant;
	}

}
