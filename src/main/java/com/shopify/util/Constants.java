package com.shopify.util;

public class Constants {

	public static final String URI = "https://jack-test-2.myshopify.com/admin/api/2021-01/products.json?fields=id,title,vendor,variants,product_type,created_at";

	public static final String ACCESS_TOKEN_STRING = "X-Shopify-Access-Token";

	public static final String PVT_AUTH_TOKEN = "37806c003c8b5be566502852a275f9e0";
}
