package com.shopify;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.shopify.dao.ProductDAO;
import com.shopify.model.ProductVO;
@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@Transactional
public class ProductServiceTest {
	
	@Autowired
	private ProductDAO productDAO;
	
	@Test
	public void insertTest() {
		ProductVO productVO = new ProductVO();
		productVO.setId("1");
		productVO.setVendor("abc");
		productVO.setTitle("def");
		productVO.setCreated_at("abc");
		productVO.setProduct_type("def");
		
		productDAO.save(productVO);
		
		Iterable<ProductVO> db = productDAO.findAll();
		assertNotNull(db);
		assertNotNull(db.iterator().next().getId());
	}

}
